package com.coupons.webbeans;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.coupons.beans.Coupon;
import com.coupons.beans.CouponType;

@XmlRootElement
public class WebCoupon implements Serializable {

	private static final long serialVersionUID = 5243328051403223122L;
	
	public static final String IMAGE_DEFAULT_TEMP_FOLDER = "images/temp/";
	public static final String IMAGE_DEFAULT_FOLDER = "images/";
	public static final String IMAGE_DEFAULT_NAME_PREFIX = "coupon_img_";
	public static final String  IMAGE_DEFAULT_EXT = ".jpg";
	//
	// Attributes
	//
	private long id;
	private String title;
	private String startDate;
	private String endDate;
	private int amount;
	private String type;
	private String message;
	private double price;
	private String image;
	
	//
	// Constructors
	//
	/**
	 * 
	 * Empty Constructor to create Coupon
	 * 
	 */
	public WebCoupon() {}
	
	/**
	 * 
	 * Constructor creating WebCoupon from provided Coupon
	 * 
	 * @param coupon
	 */
	public WebCoupon(Coupon coupon) {
			this.title = coupon.getTitle();
			this.id = coupon.getId();
			this.startDate = (coupon.getStartDate()).toString();
			this.endDate = (coupon.getEndDate()).toString();
			this.amount = coupon.getAmount();
			this.type = coupon.getType().name();
			this.message = coupon.getMessage();
			this.price = coupon.getPrice();
			this.image = coupon.getImage();
	}
	
	/**
	 * 
	 * Convert WebCoupon Object to Coupon
	 * 
	 * @return
	 */
	public Coupon convertToCoupon() {
		
		Coupon coupon = new Coupon();
		coupon.setTitle(this.getTitle());
		coupon.setId(this.getId());
		coupon.setStartDate(convertToDate(this.getStartDate()));
		coupon.setEndDate(convertToDate(this.getEndDate()));
		coupon.setAmount(this.getAmount());
		coupon.setType(CouponType.valueOf(this.getType().toUpperCase()));
		coupon.setMessage(this.getMessage());
		coupon.setPrice(this.getPrice());
		coupon.setImage(this.getImage());

		return coupon;
	}
	
	/**
	 * 
	 * This method converts Collection of WebCoupons to Coupons
	 * 
	 * @param list of Coupons
	 * @return list of WebCoupons
	 * 
	 */
	public static Collection<Coupon> convertToCoupons(Collection<WebCoupon> list) 
	{
		
		Collection<Coupon> result = new ArrayList<>();
		for (WebCoupon webCoupon : list) 
		{
			result.add(webCoupon.convertToCoupon());
		}
		return result;
	}	
	
	/**
	 * 
	 * This method converts Collection of Coupons to WebCoupons
	 * 
	 * @param list of Coupons
	 * @return list of WebCoupons
	 * 
	 */
	public static List<WebCoupon> convertToWebCoupons(Collection<Coupon> list)
	{
		List<WebCoupon> result = new ArrayList<>();
		for (Coupon coupon : list)
		{
			if (coupon != null)
			{
				result.add(new WebCoupon(coupon));
			}	
		}
		
		return result;
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public int getAmount() {
		return amount;
	}

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

	public double getPrice() {
		return price;
	}

	public String getImage() {
		return image;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setImage(String image) {
		this.image = image;
	}

	private Date convertToDate(String dateStr) {
        LocalDate date = LocalDate.parse(dateStr);
        return Date.valueOf(date);
    }

	//@Override
	//public String toString() {
	//	return "{id:" + id + ", title:" + title + ", startDate:" + startDate + ", endDate:" + endDate
	//			+ ", amount:" + amount + ", type:" + type + ", message:" + message + ", price:" + price + ", image:"
	//			+ image + "}";
	//}

	

}