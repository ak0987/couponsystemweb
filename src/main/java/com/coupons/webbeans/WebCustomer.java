package com.coupons.webbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.coupons.beans.Customer;

/**
 * Customer class provides Get/Set methods for communication between Application and DAO
 * 
 * @author A.Kugel
 *
 */
@XmlRootElement
public class WebCustomer implements Serializable {
	
	private static final long serialVersionUID = 6272047440397418304L;
	
	//
	// Attributes
	//
	private long id;
	private String customerName;
	private String password;
	private String email;
	private Collection<WebCoupon> coupons;
	
	//
	// Constructors
	//
	/**
	 * 
	 * Empty Constructor to create Customer
	 * 
	 */
	public WebCustomer() {}
	
	
	/**
	 * This Constructor creates Customer with provided parameters (name, password and email)
	 * 
	 * @param customerName represents Customer name
	 * @param password is a Customer password
	 * @param email is a Customer email
	 */
	public WebCustomer(String customerName, String password, String email) {
		this.customerName = customerName;
		this.password = password;
		this.email = email;
	}
	
	/**
	 * This Constructor creates Customer with provided parameters (id, name and email)
	 * 
	 * @param id of the Customer
	 * @param customerName Customer name
	 * @param email Customer email
	 */
	public WebCustomer(long id, String customerName, String email) {
		this.id = id;
		this.customerName = customerName;
		this.email = email;
	}
	
	/**
	 * This Constructor creates Customer with all existing details (id, name, password and email)
	 * 
	 * @param id of the Customer
	 * @param customerName Customer name
	 * @param password Customer password to access to Coupon System
	 * @param email Customer email
	 */
	public WebCustomer(long id, String customerName, String password, String email) {
		this.id = id;
		this.customerName = customerName;
		this.password = password;
		this.email = email;
	}
	
	/**
	 * This Constructor creates WebCustomer from Customer Object
	 * 
	 * @param customer Customer Object
	 */
	public WebCustomer(Customer customer) {
		this.setId(customer.getId());
		this.setCustomerName(customer.getCustomerName());
		this.setPassword(customer.getPassword());
		this.setEmail(customer.getEmail());
	}
	
	//
	// Methods
	// Convert methods between Web-Objects to DAO-Objects
	//	
	/**
	 * 
	 * Convert WebCustomer Object to Customer
	 * 
	 * @return Customer Objects
	 */
	public Customer convertToCustomer() {
		return new Customer(this.getId(), this.getCustomerName(), this.getPassword(), this.getEmail());
	}	
	
	/**
	 * 
	 * Convert provided list of Customer Objects to WebCustomer Objects
	 * 
	 * @param collection the list of Customers
	 * @return list of WebCustomers
	 */
	public static List<WebCustomer> convertToWebCustomers(Collection<Customer> collection) {
		List<WebCustomer> webCustomers = new ArrayList<>();
		
		if (collection != null) {
			for (Customer customer : collection) {
				webCustomers.add(new WebCustomer(customer));
			}
		}
		
		return webCustomers;
	}
	
	/**
	 * 
	 * Convert provided list of WebCustomer Objects to Customer Objects
	 * 
	 * @param web-customers the list of WebCustomer Objects
	 * @return list of Customers
	 */
	public static List<Customer> convertToCustomers(List<WebCustomer> webCustomers) {
		List<Customer> customers = new ArrayList<>();
		
		if (webCustomers != null) {
			for (WebCustomer webCustomer : webCustomers) {
				customers.add(webCustomer.convertToCustomer());
			}
		}
		
		return customers;
	}


	//
	// Methods
	// Getters / Setters and Override of toString method
	//
	/**
	 * Method to get Customer ID
	 * 
	 * @return Customer ID
	 */
	public long getId() {
		return id;
	}

	/**
	 * Set Customer ID
	 * 
	 * @param id id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Get the name of the Customer
	 * 
	 * @return name of the Customer
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * Update the Customer name 
	 * 
	 * @param customerName name of the Customer
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * Get the Customer password
	 * 
	 * @return current Customer password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Set a new password for a Customer
	 * 
	 * @param password password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Get Customer email
	 * 
	 * @return Customer email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Set a new email for a Customer
	 * 
	 * @param email email to update Customer email field
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Get Collection of Coupons
	 * 
	 * @return coupons Collection
	 */
	public Collection<WebCoupon> getCoupons() {
		return coupons;
	}

	/**
	 * Set Collection of Coupons to Customer
	 * 
	 * @param coupons list of Coupons
	 */
	public void setCoupons(Collection<WebCoupon> coupons) {
		this.coupons = coupons;
	}

	@Override
	/**
	 * 
	 * This method returns Customer id, name and email
	 * 
	 */
	public String toString() {
		return "\nCustomer "
			  + "[id=" + this.id + ", " 
			  + "Name: " + this.customerName + ", " 
			  + "email: " + this.email + "]";
	}

}
