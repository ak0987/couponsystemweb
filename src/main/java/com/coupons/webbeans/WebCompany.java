package com.coupons.webbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.coupons.beans.Company;
import com.coupons.beans.Coupon;

@XmlRootElement
public class WebCompany implements Serializable {

	private static final long serialVersionUID = 7703952885257843315L;
	
	//
	// Attributes
	//
	private long id;
	private String companyName;
	private String password;
	private String email;
	private Collection<Coupon> coupons;
	
	// 
	// Constructors
	//
	/**
	 * 
	 * Empty Constructor to create Company
	 * 
	 */
	public WebCompany() {}
	
	
	/**
	 * This Constructor creates Company with provided parameters (name, password and email)
	 * 
	 * @param companyName represents Company name
	 * @param password is a Company password
	 * @param email is a Company email
	 */
	public WebCompany(String companyName, String password, String email) {
		this.companyName = companyName;
		this.password = password;
		this.email = email;
	}
	
	/**
	 * This Constructor creates Company with provided parameters (id, name and email)
	 * 
	 * @param id of the Company
	 * @param companyName Company name
	 * @param email Company email
	 */
	public WebCompany(long id, String companyName, String email) {
		this.id = id;
		this.companyName = companyName;
		this.email = email;
	}

	/**
	 * This Constructor creates WebCompany from Company Object
	 * 
	 * @param company Company Object
	 */
	public WebCompany(Company company) {
		this.setId(company.getId());
		this.setCompanyName(company.getCompanyName());
		this.setPassword(company.getPassword());
		this.setEmail(company.getEmail());
	}
	
	//
	// Methods
	// Convert methods between Web-Objects to DAO-Objects
	//	
	/**
	 * 
	 * Convert WebCompany Object to Company
	 * 
	 * @return Company Objects
	 */
	public Company convertToCompany() {
		return new Company(this.getId(), this.getCompanyName(), this.getPassword(), this.getEmail());
	}	
	
	/**
	 * 
	 * Convert provided list of Company Objects to WebCompany Objects
	 * 
	 * @param company the list of Company
	 * @return list of WebCompanys
	 */
	public static List<WebCompany> convertToWebCompanies(Collection<Company> collection) {
		List<WebCompany> result = new ArrayList<>();
		
		if (collection != null) {
			for (Company company : collection) {
				result.add(new WebCompany(company));
			}
		}
		
		return result;
	}
	
	/**
	 * 
	 * Convert provided list of WebCompany Objects to Company Objects
	 * 
	 * @param web-company the list of WebCompany Objects
	 * @return list of Company
	 */
	public static List<Company> convertToCompany(List<WebCompany> webCompanies) {
		List<Company> company = new ArrayList<>();
		
		if (webCompanies != null) {
			for (WebCompany webCompany : webCompanies) {
				company.add(webCompany.convertToCompany());
			}
		}
		
		return company;
	}
	
	//
	// Methods
	// Getters / Setters and Override of toString method
	//	
	/**
	 * Method to get Company ID
	 * 
	 * @return Company ID
	 */
	public long getId() {
		return id;
	}

	/**
	 * Set Company ID
	 * 
	 * @param id id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Get the name of the Company
	 * 
	 * @return name of the Company
	 */
	public String getCompanyName() {
		return this.companyName;
	}

	/**
	 * Update the Company name 
	 * 
	 * @param companyName name of the Company
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * Get the Company password
	 * 
	 * @return current Company password
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Set a new password for a Company
	 * 
	 * @param password password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Get Company email
	 * 
	 * @return Company email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Set a new email for a Company
	 * 
	 * @param email email to update Company email field
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Get Collection of Coupons
	 * 
	 * @return coupons Collection
	 */
	public Collection<Coupon> getCoupons() {
		return this.coupons;
	}

	/**
	 * Set Collection of Coupons to Company
	 * 
	 * @param coupons list of Coupons
	 */
	public void setCoupons(Collection<Coupon> coupons) {
		this.coupons = coupons;
	}	
	
	@Override
	/**
	 * 
	 * This method returns Company id, name and email
	 * 
	 */
	public String toString() {
		return "\nCompany"
			 + "[id=" + this.id + ", " 
			 + "Name: " + this.companyName + ", " 
			 + "email: " + this.email + "]";
	}
}
