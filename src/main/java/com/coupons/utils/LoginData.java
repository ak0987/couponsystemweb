package com.coupons.utils;


import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class LoginData implements Serializable {

	private static final long serialVersionUID = -214971180422621715L;
	
	private String username;
	private String password;
	private String userType;
	
	public LoginData() {}
	
	
	public LoginData(String username, String password, String userType) {
		this.username = username;
		this.password = password;
		this.userType = userType;

	}
	
	
	/**
	 * Get Username
	 * 
	 * @return Username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * Set Username 
	 * 
	 * @param username Username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Get password
	 * 
	 * @return password
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Set a new password
	 * 
	 * @param password password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Get User Type
	 * 
	 * @return User Type
	 */
	public String getUserType() {
		return this.userType;
	}

	/**
	 * Set User Type
	 * 
	 * @param userType User Type
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

}
