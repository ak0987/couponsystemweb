package com.coupons.utils;

import java.sql.Date;
import java.time.LocalDate;

public final class Helpers {
	
	
	//Build JSON message for error notifications
	public static String JsonMessage(String messageId, String messageBody) {
		return "{ \"" + messageId + "\" : \"" + messageBody.replace('\n', ' ') + "\"}";
	}
	
	
	//Convert string date representation to LocalDate
	public static Date ConvertToDate(String dateStr) 
	{
        LocalDate date = LocalDate.parse(dateStr);
        return Date.valueOf(date);
    }

}
