package com.coupons.services;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.coupons.beans.Company;
import com.coupons.beans.Customer;
import com.coupons.exceptions.CouponSystemException;
import com.coupons.exceptions.DAOException;
import com.coupons.exceptions.DuplicateEntryException;
import com.coupons.exceptions.InvalidEntryException;
import com.coupons.facades.ClientType;
import com.coupons.facades.AdminFacade;
import com.coupons.facades.CouponSystemSingleton;
import com.coupons.webbeans.WebCompany;
import com.coupons.webbeans.WebCustomer;
import com.coupons.utils.Helpers;

@Path("admin")
public class AdminService {
		
	private AdminFacade adminFacade;
	private final String LOG_IN_FAILED_MESSAGE = "LOGIN FAILED!";
	
	@POST
	@Path("/create-company")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response createCompany(WebCompany webCompany,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        System.out.println(webCompany);
        if (getFacade(username, password, userType) != null) {
            try {
            	adminFacade.createCompany(webCompany.convertToCompany());
            	return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
        
    }
	
	@DELETE
	@Path("/remove-company")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeCompany(@QueryParam("id") long id,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
	{
		if (getFacade(username, password, userType) != null) {
			try {
				Company company = adminFacade.getCompany(id);
				adminFacade.removeCompany(company);
				return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
			} catch (InvalidEntryException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}
		
		
		
	}
	
	@PUT
	@Path("/update-company")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCompany(WebCompany webCompany,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
	{
		if (getFacade(username, password, userType) != null) {
			try {
				adminFacade.updateCompany(webCompany.convertToCompany());
				return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
			} catch (DuplicateEntryException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}
		
	}
	

	@GET
	@Path("/get-company")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCompany(@QueryParam("id") long id,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        WebCompany result = null;
        if (getFacade(username, password, userType) != null) {
            try {
                result = new WebCompany(adminFacade.getCompany(id));
                return Response.ok(result, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}
        
    }
	
	@GET
    @Path("/get-all-companies")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCompanies(@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        List<WebCompany> result = null;
        if (getFacade(username, password, userType) != null) {
            try {
            	result = WebCompany.convertToWebCompanies(adminFacade.getAllCompanies());
            	return Response.ok(new GenericEntity<List<WebCompany>>(result) {}, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}

    }	
	
	@POST
	@Path("/create-customer")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response createCustomer(WebCustomer webCustomer,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        System.out.println(webCustomer);
        if (getFacade(username, password, userType) != null) {
            try {
            	adminFacade.createCustomer(webCustomer.convertToCustomer());
            	return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}

    }
	
	@DELETE
	@Path("/remove-customer")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeCustomer(@QueryParam("id") long id,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
	{
		if (getFacade(username, password, userType) != null) {
			try {
				Customer customer = adminFacade.getCustomer(id);
				adminFacade.removeCustomer(customer);
				return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
			} catch (InvalidEntryException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}
		
	}
	
	@PUT
	@Path("/update-customer")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCoupon(WebCustomer webCustomer,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
	{
		if (getFacade(username, password, userType) != null) {
			try {
				adminFacade.updateCustomer(webCustomer.convertToCustomer());
				return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
			} catch (DuplicateEntryException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}
	}

	@GET
	@Path("/get-customer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@QueryParam("id") long id,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        WebCustomer result = null;
        if (getFacade(username, password, userType) != null) {
            try {
                result = new WebCustomer(adminFacade.getCustomer(id));
                return Response.ok(result, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}
    }
	
	@GET
    @Path("/get-all-customers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCustomers(@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        List<WebCustomer> result = null;
        if (getFacade(username, password, userType) != null) {
            try {
            	result = WebCustomer.convertToWebCustomers(adminFacade.getAllCustomers());
            	return Response.ok(new GenericEntity<List<WebCustomer>>(result) {}, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
        	
			return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
		}
    }	
	
	//Get Administrator facade instance
	private AdminFacade getFacade(String username, String password, String userType) throws SQLException {
        if (adminFacade == null){
            try {
            	if (userType.equalsIgnoreCase(ClientType.ADMIN.toString())) {
            		adminFacade = (AdminFacade) CouponSystemSingleton.getInstance().login(username, password, ClientType.ADMIN);
            	}
            	else {
            		return null;
            	}
            	
            } catch (CouponSystemException exception) {
            	exception.printStackTrace();
            }
        }
        return adminFacade;
    }
	
	
	
}
