package com.coupons.services;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;


import com.coupons.exceptions.CouponSystemException;
import com.coupons.facades.ClientType;
import com.coupons.facades.CompanyFacade;
import com.coupons.facades.CouponClientFacade;
import com.coupons.facades.AdminFacade;
import com.coupons.facades.CouponSystemSingleton;
import com.coupons.facades.CustomerFacade;
import com.coupons.utils.Helpers;
import com.coupons.utils.LoginData;


@Path("")
public class LoginService {

	
	private final String LOG_IN_FAILED_MESSAGE = "LOGIN FAILED!";
	
	@POST
	@Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response Login(LoginData loginData) throws SQLException
    {
		
		try {
			
			CouponSystemSingleton singletonCouponSystem = CouponSystemSingleton.getInstance();
			CouponClientFacade facade = null;
			
			String url = "";
			
			if (loginData.getUserType().equalsIgnoreCase("Admin")){
				facade = (AdminFacade) singletonCouponSystem.login(loginData.getUsername(), loginData.getPassword(), ClientType.ADMIN );
				if (facade != null){
					url = "admin";
				}
				else {
					url = "";
				}
			}
			else if (loginData.getUserType().equalsIgnoreCase("Company")){
				facade = (CompanyFacade) singletonCouponSystem.login(loginData.getUsername(), loginData.getPassword(), ClientType.COMPANY );
				if (facade != null){
					url = "company";
				}
				else {
					url = "";
				}
			}
			else if (loginData.getUserType().equalsIgnoreCase("Customer")){
				facade = (CustomerFacade) singletonCouponSystem.login(loginData.getUsername(), loginData.getPassword(), ClientType.CUSTOMER );
				if (facade != null){
					url = "customer";
				}
				else {
					url = "";
				}
			}
			else {
				url = "";
			}
			
			if (facade != null) {
				return Response
						.ok(Helpers.JsonMessage("url", url), MediaType.APPLICATION_JSON)
			            .cookie(new NewCookie("username", loginData.getUsername()))
			            .cookie(new NewCookie("password", loginData.getPassword()))
			            .cookie(new NewCookie("userType", loginData.getUserType()))
			            .build();
			}
			else {
				return Response
			            .ok(Helpers.JsonMessage("error", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON)
			            .cookie(new NewCookie("username", ""))
			            .cookie(new NewCookie("password", ""))
			            .cookie(new NewCookie("userType", ""))
			            .build();
			}
			
		}
		catch (CouponSystemException e) {
			return Response
		            .ok(Helpers.JsonMessage("error", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON)
		            .cookie(new NewCookie("username", ""))
		            .cookie(new NewCookie("password", ""))
		            .cookie(new NewCookie("userType", ""))
		            .build();
		}

    }
	
	
	
	@GET
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
    public Response LogOut()
    {	
		String url;
		
		url = "../";
		
		return Response
				.ok(Helpers.JsonMessage("url", url), MediaType.APPLICATION_JSON)
	            .cookie(new NewCookie("username", ""))
	            .cookie(new NewCookie("password", ""))
	            .cookie(new NewCookie("userType", ""))
	            .build();
    }
	
	
}