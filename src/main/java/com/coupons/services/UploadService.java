package com.coupons.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
//import org.glassfish.jersey.media.multipart.MultiPartFeature;

import com.coupons.webbeans.WebCoupon;


@Path("")
public class UploadService {
	
	@POST
	@Path("/upload-file")
	@Consumes({MediaType.MULTIPART_FORM_DATA})
	public Response uploadFile(  @FormDataParam("file") InputStream fileInputStream,
	                                @FormDataParam("file") FormDataContentDisposition fileMetaData,
	                                @FormDataParam("couponId") int couponId) throws Exception
	{
			
		if (fileMetaData.getFileName() != null) {
			
			String couponImageFileName = "";
			
		    try
		    {
		        int read = 0;
		        byte[] bytes = new byte[1024];
		        long imageUniqueId = Calendar.getInstance().getTime().getTime();
		        
		        couponImageFileName = WebCoupon.IMAGE_DEFAULT_NAME_PREFIX + imageUniqueId + WebCoupon.IMAGE_DEFAULT_EXT;
		 
		        OutputStream out = new FileOutputStream(new File(WebCoupon.IMAGE_DEFAULT_TEMP_FOLDER + couponImageFileName));
		        while ((read = fileInputStream.read(bytes)) != -1)
		        {
		            out.write(bytes, 0, read);
		        }
		        out.flush();
		        out.close();
		    } catch (IOException e)
		    {
		        throw new WebApplicationException("Error while uploading file. Please try again !!");
		    }
		    return Response.ok(couponImageFileName).build();
		}

	    return Response.ok("").build();
	}
}
