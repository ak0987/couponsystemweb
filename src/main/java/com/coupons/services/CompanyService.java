package com.coupons.services;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static java.nio.file.StandardCopyOption.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.coupons.beans.CouponType;
import com.coupons.exceptions.CouponSystemException;
import com.coupons.exceptions.DAOException;
import com.coupons.exceptions.DuplicateEntryException;
import com.coupons.exceptions.InvalidEntryException;
import com.coupons.facades.ClientType;
import com.coupons.facades.CompanyFacade;
import com.coupons.facades.CouponSystemSingleton;
import com.coupons.webbeans.WebCoupon;
import com.coupons.utils.Helpers;

@Path("company")
public class CompanyService {
		
	private CompanyFacade companyFacade;
	private final String LOG_IN_FAILED_MESSAGE = "LOGIN FAILED!";
		
	@POST
	@Path("/create-coupon")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response createCoupon(WebCoupon webCoupon,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        System.out.println(webCoupon);
        if(getFacade(username, password, userType) != null) {
            try {
            	String img = webCoupon.getImage();
				if (img.contains("$")) {
					swapImageFiles(webCoupon);
				}
            	companyFacade.createCoupon(webCoupon.convertToCoupon());
            	return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            } catch (IOException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
        }
        else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
    }
	
	@DELETE
	@Path("/remove-coupon")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeCoupon(@QueryParam("id") long id,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
	{
		if (getFacade(username, password, userType) != null) {
			try {
				WebCoupon webCoupon = new WebCoupon(companyFacade.getCoupon(id));
				companyFacade.removeCoupon(webCoupon.convertToCoupon());
				return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
			} catch (InvalidEntryException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
	}
	
	@PUT
	@Path("/update-coupon")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCoupon(WebCoupon webCoupon,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
	{
		if (getFacade(username, password, userType) != null) {
			try {
				String img = webCoupon.getImage();
				if (img.contains("$")) {
					swapImageFiles(webCoupon);
				}
				companyFacade.updateCoupon(webCoupon.convertToCoupon());
				return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
			} catch (DuplicateEntryException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} catch (IOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} 
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }

	}

	@GET
	@Path("/get-coupon")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCoupon(@QueryParam("id") long id,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws  SQLException
    {
        WebCoupon result = null;
        if (getFacade(username, password, userType) != null) {
            try {
                result = new WebCoupon(companyFacade.getCoupon(id));
                return Response.ok(result, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	System.out.println(e.getMessage());
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
    }
	
	@GET
    @Path("/get-all-coupons")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCoupons(@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        List<WebCoupon> result = null;
        if (getFacade(username, password, userType) != null) {
            try {
            	result = WebCoupon.convertToWebCoupons(companyFacade.getCompanyCoupons());
                return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        } 
    }
	
	
	
	
	@GET
    @Path("/get-coupons-list-query")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCouponsByQuery(@QueryParam("couponTypeFilter") String couponTypeFilter, 
			@QueryParam("couponDateFiler") String couponDateFiler, 
			@QueryParam("couponPriceFilter") int couponPriceFilter,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        List<WebCoupon> result = null;
        
        if (getFacade(username, password, userType) != null) {
            try {
            	result = getCopmpanyCouponsByQuery(couponTypeFilter, couponDateFiler, couponPriceFilter);
                return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
        else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
    }
	
	
	
	@GET
	@Path("/coupons-by-type")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCouponsByType(@QueryParam("type") String type,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException {
		List<WebCoupon> result = null;
		if (getFacade(username, password, userType) != null) {
			try {
				result = WebCoupon.convertToWebCoupons(companyFacade.getCompanyCouponsByType(CouponType.valueOf(type)));
				return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
	}
	
	@GET
	@Path("/coupons-by-max-price")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCouponsByPrice(@QueryParam("price") double price,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException {
		List<WebCoupon> result = null;
		if (getFacade(username, password, userType) != null) {
			try {
				result = WebCoupon.convertToWebCoupons(companyFacade.getCompanyCouponsByPrice(price));
				return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
	}
	
	@GET
	@Path("/coupons-by-date")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCouponsByDate(@QueryParam("date") String date,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException {
		List<WebCoupon> result = null;
		if (getFacade(username, password, userType) != null) {
			try {
				result = WebCoupon.convertToWebCoupons(companyFacade.getCompanyCouponsByDate(Helpers.ConvertToDate(date)));
				return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
	}	
	
	
	@GET
	@Path("/get-company-name")
	@Produces(MediaType.APPLICATION_JSON)
    public Response getCompanyName(@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
		String result = "";
		
		if (getFacade(username, password, userType) != null) {
			result = companyFacade.getLoggedInCompanyName();
			return Response.ok(Helpers.JsonMessage("companyName", result), MediaType.APPLICATION_JSON).build();
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
		
    }
	
	
	
	//Get Company facade instance
	private CompanyFacade getFacade(String username, String password, String userType) throws SQLException {
        if (companyFacade == null){
            try {           	
            	if (userType.equalsIgnoreCase(ClientType.COMPANY.toString())) {
            		companyFacade = (CompanyFacade) CouponSystemSingleton.getInstance().login(username, password, ClientType.COMPANY);
            	}
            	else {
            		return null;
            	}
            } catch (CouponSystemException exception) {
            	exception.printStackTrace();
            }
        }
        return companyFacade;
    }
	
	
	//Return Company coupons by "complex" query
	private List<WebCoupon> getCopmpanyCouponsByQuery(String couponTypeFilter, String couponDateFiler, int couponPriceFilter) throws CouponSystemException {
		
		List<WebCoupon> result = new ArrayList<>();
		List<ArrayList<WebCoupon>> couponLists = new ArrayList<ArrayList<WebCoupon>>();
		Boolean filterApplied = false;
		
		if (!couponTypeFilter.equals("")) {
			List<WebCoupon> typeList = WebCoupon.convertToWebCoupons(companyFacade.getCompanyCouponsByType(CouponType.valueOf(couponTypeFilter)));
			filterApplied = true;
			if(typeList != null && typeList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) typeList);
			}
			
		}
		
		
		if (!couponDateFiler.equals("")) {
			Date couponDateFilerDate = Helpers.ConvertToDate(couponDateFiler);
			List<WebCoupon> dateList = WebCoupon.convertToWebCoupons(companyFacade.getCompanyCouponsByDate(couponDateFilerDate));
			filterApplied = true;
			if(dateList != null && dateList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) dateList);
			}
		}		
			
		
		if (couponPriceFilter > 0) {
			List<WebCoupon> priceList = WebCoupon.convertToWebCoupons(companyFacade.getCompanyCouponsByPrice(couponPriceFilter));
			filterApplied = true;
			if(priceList != null && priceList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) priceList);
			}
		}
		
		
		if (couponLists.size() > 0) {
			result = getArraysIntersection(couponLists);
		}
		else if (!filterApplied){
			result = WebCoupon.convertToWebCoupons(companyFacade.getCompanyCoupons());
		}

		return result;
	}
	
	
	//Get intersection array of WebCoupon arrays by ID (unique).
	//All included arrays must contain at least one element.
	private List<WebCoupon> getArraysIntersection(List<ArrayList<WebCoupon>> couponLists) {
		
		List<WebCoupon> result = new ArrayList<WebCoupon>();
		
		if (couponLists != null && couponLists.size() > 0) {
			
			if (couponLists.size() > 1) {
				
				//Get the first array in the list, it's elements will be tested
				//against elements in the rest of the arrays.
				List<WebCoupon> initList = couponLists.get(0);
				
				//Get each coupon from initList
				for (int i = 0; i < initList.size(); i++) {
					
					WebCoupon coupon = initList.get(i);
					Boolean found = true;
					
					//Try to find coupon in all other arrays
					for(int k = 1; k < couponLists.size(); k++) {		
						if (findCouponInArray(coupon, couponLists.get(k)) == null) {
							found = false;
							break;
						}
					}
					
					if (found) {
						result.add(coupon);
					}
					
				}

			}
			else {
				result = couponLists.get(0);
			}
		}
			
		return result;
	}
	
	//Find coupon in coupon array (by ID)
	private WebCoupon findCouponInArray(WebCoupon coupon, ArrayList<WebCoupon> couponList) {
		
		long couponId = coupon.getId();
		
		for (int i = 0; i < couponList.size(); i++) {
			
			if (couponList.get(i).getId() == couponId) {
				return couponList.get(i);
			}
		}
			
		return null;
	}

	//Swap updated image from temp folder to regular image folder and update "image" property
	private void swapImageFiles(WebCoupon webCoupon) throws IOException {
		//move image files from temp to work path, set final image file URL
		
		String[] nameArr = webCoupon.getImage().split("\\$");
		
		String oldCouponImageFileName = nameArr[0];
		String newCouponImageFileName = nameArr[1];
		
		//Delete old file
		if (!oldCouponImageFileName.equals("")){
			String[] oldNameArr = oldCouponImageFileName.split("/");
			java.nio.file.Path oldImageFile = Paths.get(WebCoupon.IMAGE_DEFAULT_FOLDER + oldNameArr[oldNameArr.length - 1]);
			Files.delete(oldImageFile);
		}
		
		java.nio.file.Path src = Paths.get(WebCoupon.IMAGE_DEFAULT_TEMP_FOLDER + newCouponImageFileName);
		java.nio.file.Path dest = Paths.get(WebCoupon.IMAGE_DEFAULT_FOLDER + newCouponImageFileName);

		Files.move(src, dest, REPLACE_EXISTING);
		webCoupon.setImage("images/" + newCouponImageFileName);

	}
	
	
}
