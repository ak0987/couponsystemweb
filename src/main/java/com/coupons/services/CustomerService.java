package com.coupons.services;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.coupons.beans.CouponType;
import com.coupons.exceptions.CouponSystemException;
import com.coupons.exceptions.DAOException;
import com.coupons.facades.ClientType;
import com.coupons.facades.CouponSystemSingleton;
import com.coupons.facades.CustomerFacade;
import com.coupons.webbeans.WebCoupon;
import com.coupons.utils.Helpers;

@Path("customer")
public class CustomerService {
		
	private CustomerFacade customerFacade;
	private final String LOG_IN_FAILED_MESSAGE = "LOGIN FAILED!";

	@GET
	@Path("/get-coupon")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCoupon(@QueryParam("id") long id,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        WebCoupon result = null;
        if (getFacade(username, password, userType) != null) {
            try {
                result = new WebCoupon(customerFacade.getCoupon(id));
                return Response.ok(result, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
    }
	
	@GET
    @Path("/get-all-purchased-coupons")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPurchasedCoupons(@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        List<WebCoupon> result = null;
        if (getFacade(username, password, userType) != null) {
            try {
            	result = WebCoupon.convertToWebCoupons(customerFacade.getAllPurchasedCoupons());
            	 return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
    }
	
	@GET
    @Path("/get-all-coupons")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCoupons(@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        List<WebCoupon> result = null;
        if (getFacade(username, password, userType) != null) {
            try {
            	result = WebCoupon.convertToWebCoupons(customerFacade.getAllCoupons());
            	 return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
    }
	
	@PUT
	@Path("/purchase-coupon")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response purchaseCoupon(WebCoupon webCoupon,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
	{
		if (getFacade(username, password, userType) != null) {
			try {
				customerFacade.purchaseCoupon(webCoupon.convertToCoupon());
				return Response.ok(Helpers.JsonMessage("success", "ok"), MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			} 
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
	}
	
	@GET
    @Path("/get-coupons-list-query")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCouponsByQuery(@QueryParam("couponTypeFilter") String couponTypeFilter, 
			@QueryParam("couponDateFiler") String couponDateFiler, 
			@QueryParam("couponPriceFilter") double couponPriceFilter,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        List<WebCoupon> result = null;
        
        if (getFacade(username, password, userType) != null) {
            try {
            	result = getCouponListByQuery(couponTypeFilter, couponDateFiler, couponPriceFilter);
            	return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }

    }
	
	
	@GET
    @Path("/get-customer-coupons-list-query")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerCouponsByQuery(@QueryParam("couponTypeFilter") String couponTypeFilter, 
			@QueryParam("couponDateFiler") String couponDateFiler, 
			@QueryParam("couponPriceFilter") double couponPriceFilter,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
        List<WebCoupon> result = null;
        
        if (getFacade(username, password, userType) != null) {
            try {
            	result = getCustomerCouponListByQuery(couponTypeFilter, couponDateFiler, couponPriceFilter);
            	 return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
            } catch (CouponSystemException e) {
            	return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
            }
        }
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
    }
	
	@GET
	@Path("/coupons-by-type")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCouponsByType(@QueryParam("type") String type,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException {
		List<WebCoupon> result = null;
		if (getFacade(username, password, userType) != null) {
			try {
				result = WebCoupon.convertToWebCoupons(customerFacade.getPurchasedCouponsByType((CouponType.valueOf(type))));
				return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
	}
	
	@GET
	@Path("/coupons-by-max-price")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCouponsByPrice(@QueryParam("price") double price,
    		@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException {
		List<WebCoupon> result = null;
		if (getFacade(username, password, userType) != null) {
			try {
				result = WebCoupon.convertToWebCoupons(customerFacade.getPurchasedCouponsByPrice(price));
				return Response.ok(new GenericEntity<List<WebCoupon>>(result) {}, MediaType.APPLICATION_JSON).build();
			} catch (DAOException e) {
				return Response.ok(Helpers.JsonMessage("error", e.getMessage()), MediaType.APPLICATION_JSON).build();
			}
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
	}
	
	
	@GET
	@Path("/get-customer-name")
	@Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerName(@CookieParam("username") String username,
    		@CookieParam("password") String password,
    		@CookieParam("userType") String userType) throws SQLException
    {
		String result = "";
		
		if (getFacade(username, password, userType) != null) {
			result = customerFacade.getLoggedInCustomerName();
			return Response.ok(Helpers.JsonMessage("customerName", result), MediaType.APPLICATION_JSON).build();
		}
		else {
        	return Response.ok(Helpers.JsonMessage("redirectToLogin", LOG_IN_FAILED_MESSAGE), MediaType.APPLICATION_JSON).build();
        }
    }
	
	//Get Customer facade instance
	private CustomerFacade getFacade(String username, String password, String userType) throws SQLException {
        if (customerFacade == null){
            try {
            	if (userType.equalsIgnoreCase(ClientType.CUSTOMER.toString())) {
            		customerFacade = (CustomerFacade) CouponSystemSingleton.getInstance().login(username, password, ClientType.CUSTOMER);
            	}
            	else {
            		return null;
            	}
            } catch (CouponSystemException exception) {
            	exception.printStackTrace();
            }
        }
        return customerFacade;
    }
	

	//Return Customer coupons by "complex" query
	private List<WebCoupon> getCustomerCouponListByQuery(String couponTypeFilter, String couponDateFiler, double couponPriceFilter) throws CouponSystemException {
			
		List<WebCoupon> result = new ArrayList<>();
		List<ArrayList<WebCoupon>> couponLists = new ArrayList<ArrayList<WebCoupon>>();
		Boolean filterApplied = false;
		
		if (!couponTypeFilter.equals("")) {
			List<WebCoupon> typeList = WebCoupon.convertToWebCoupons(customerFacade.getPurchasedCouponsByType(CouponType.valueOf(couponTypeFilter)));
			filterApplied = true;
			if(typeList != null && typeList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) typeList);
			}
			
		}
		
		
		if (!couponDateFiler.equals("")) {
			Date couponDateFilerDate = Helpers.ConvertToDate(couponDateFiler);
			List<WebCoupon> dateList = WebCoupon.convertToWebCoupons(customerFacade.getPurchasedCouponsByDate(couponDateFilerDate));
			filterApplied = true;
			if(dateList != null && dateList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) dateList);
			}
		}		
			
		
		if (couponPriceFilter > 0) {
			List<WebCoupon> priceList = WebCoupon.convertToWebCoupons(customerFacade.getPurchasedCouponsByPrice(couponPriceFilter));
			filterApplied = true;
			if(priceList != null && priceList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) priceList);
			}
		}
		
		
		if (couponLists.size() > 0) {
			result = getArraysIntersection(couponLists);
		}
		else if (!filterApplied){
			result = WebCoupon.convertToWebCoupons(customerFacade.getAllPurchasedCoupons());
		}

		return result;
	}
	
	
	//Return Coupons by "complex" query
	private List<WebCoupon> getCouponListByQuery(String couponTypeFilter, String couponDateFiler, double couponPriceFilter) throws CouponSystemException {
			
		List<WebCoupon> result = new ArrayList<>();
		List<ArrayList<WebCoupon>> couponLists = new ArrayList<ArrayList<WebCoupon>>();
		Boolean filterApplied = false;
		
		if (!couponTypeFilter.equals("")) {
			List<WebCoupon> typeList = WebCoupon.convertToWebCoupons(customerFacade.getCouponsByType(CouponType.valueOf(couponTypeFilter)));
			filterApplied = true;
			if(typeList != null && typeList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) typeList);
			}
			
		}
		
		if (!couponDateFiler.equals("")) {
			Date couponDateFilerDate = Helpers.ConvertToDate(couponDateFiler);
			List<WebCoupon> dateList = WebCoupon.convertToWebCoupons(customerFacade.getCouponsByDate(couponDateFilerDate));
			filterApplied = true;
			if(dateList != null && dateList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) dateList);
			}
		}		
			
		
		if (couponPriceFilter > 0) {
			List<WebCoupon> priceList = WebCoupon.convertToWebCoupons(customerFacade.getCouponsByPrice(couponPriceFilter));
			filterApplied = true;
			if(priceList != null && priceList.size() > 0) {
				couponLists.add((ArrayList<WebCoupon>) priceList);
			}
		}
		
		
		if (couponLists.size() > 0) {
			result = getArraysIntersection(couponLists);
		}
		else if (!filterApplied){
			result = WebCoupon.convertToWebCoupons(customerFacade.getAllCoupons());
		}
		

		return result;
	}
		
	
	//Get intersection array of WebCoupon arrays by ID (unique).
	//All included arrays must contain at least one element.
	private List<WebCoupon> getArraysIntersection(List<ArrayList<WebCoupon>> couponLists) {
			
		List<WebCoupon> result = new ArrayList<WebCoupon>();
		
		if (couponLists != null && couponLists.size() > 0) {
			
			if (couponLists.size() > 1) {
				
				//Get the first array in the list, it's elements will be tested
				//against elements in the rest of the arrays.
				List<WebCoupon> initList = couponLists.get(0);
				
				//Get each coupon from initList
				for (int i = 0; i < initList.size(); i++) {
					
					WebCoupon coupon = initList.get(i);
					Boolean found = true;
					
					//Try to find coupon in all other arrays
					for(int k = 1; k < couponLists.size(); k++) {		
						if (findCouponInArray(coupon, couponLists.get(k)) == null) {
							found = false;
							break;
						}
					}
					
					if (found) {
						result.add(coupon);
					}
					
				}

			}
			else {
				result = couponLists.get(0);
			}
		}
			
		return result;
	}
		
	
	//Find coupon in coupon array (by ID)
	private WebCoupon findCouponInArray(WebCoupon coupon, ArrayList<WebCoupon> couponList) {
			
		long couponId = coupon.getId();
		
		for (int i = 0; i < couponList.size(); i++) {
			
			if (couponList.get(i).getId() == couponId) {
				return couponList.get(i);
			}
		}
			
		return null;
	}

	
	
}
