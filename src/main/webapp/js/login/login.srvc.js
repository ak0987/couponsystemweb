app.service("LoginSrvc", LoginSrvcCtor);

function LoginSrvcCtor($http)
{
	
	this.login = function(loginData) {
		return $http.post("webapi/login", loginData);		
	}
	
	this.logout = function() {
		return $http.get("../webapi/logout");		
	}
	
	
}