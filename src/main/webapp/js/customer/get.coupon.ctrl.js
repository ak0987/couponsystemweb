app.controller("GetCouponCtrl", ['$scope', 'CustomerSrvc', '$stateParams', '$location',  '$window', 'growl', GetCouponCtrlCtor]);

// Ctor method
function GetCouponCtrlCtor($scope, CustomerSrvc, $stateParams, $location, $window, growl){
	this.coupon = {};

	var promiseGet = CustomerSrvc.getCoupon($stateParams.id);
	
	promiseGet.then(
		function (resp){
			
			if (resp.data.error) {
				growl.error(resp.data.error);
			}
			else if (resp.data.redirectToLogin) {
				$window.location.href = "../";
			}
			else {
				$scope.coupon = resp.data;
				$scope.couponImage = $scope.coupon.image;
				$scope.startDate = new Date($scope.coupon.startDate);
				$scope.endDate = new Date($scope.coupon.endDate);
			}

		},
		
		function (err){			
			alert(err.data);
		}
	)
	
			$scope.purchaseCoupon = function () {
		
		var promisePost = CustomerSrvc.purchaseCoupon($scope.coupon);
		
		promisePost.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else {
						growl.success(coupon.title + " purchased successfully");
						$location.url('get-customer-coupons');
					}
				},
				
				function (err){			
					alert(err.data);
				}
			)
			
	}

}