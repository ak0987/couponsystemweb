var app = angular.module("customerApp", ["ui.router", "angular-growl", "ngCookies"]);

app.config(['growlProvider', function(growlProvider) {
  growlProvider.globalTimeToLive(5000);
  growlProvider.globalDisableCountDown(true);
}]);