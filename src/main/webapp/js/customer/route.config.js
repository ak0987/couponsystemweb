app.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
}]);

// router config
app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state("getAllCoupons", {
        url: "/get-all-coupons",
        templateUrl: "get-all-coupons.html",
        controller: "GetAllCouponsCtrl as getAll"
    })
    .state("GetCustomerCouponsCtrl", {
        url: "/get-customer-coupons",
        templateUrl: "get-customer-coupons.html",
        controller: "GetCustomerCouponsCtrl as getCustomerCoupons"
    })
    .state("getCoupon", {
        url: "/coupon-details?id",
        templateUrl: "coupon-details.html",
        controller: "GetCouponCtrl as getCoup"
    })
     .state("purchaseCoupon", {
        url: "/purchase-coupon?id",
        templateUrl: "purchase-coupon.html",
        controller: "PurchaseCouponCtrl as purchaseCoup"
    });
    
});