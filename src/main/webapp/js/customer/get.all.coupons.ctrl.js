app.controller("GetAllCouponsCtrl", ['$scope', 'CustomerSrvc', '$window', 'growl', '$location', GetAllCouponsCtrlCtor]);

// Ctor method
function GetAllCouponsCtrlCtor($scope, CustomerSrvc, $window, growl, $location){	
	$scope.coupons = [];		
	
	populateCouponTable($scope, CustomerSrvc, $window, growl, $location)
	

	$scope.getAllCoupons = function () {
		$scope.couponTypeFilter = "";
		$scope.couponDateFiler = null;
		$scope.couponPriceFilter = "";
		populateCouponTable($scope, CustomerSrvc, $window, growl, $location);
	}
	
	$scope.getCouponListByQuery = function () {
		var couponTypeFilter = "";
		var couponDateFiler = "";
		var couponPriceFilter = "-1";
		
		if ($scope.couponTypeFilter) {
			couponTypeFilter = $scope.couponTypeFilter
		}
		
		if ($scope.couponDateFiler) {
			//couponDateFiler = $scope.couponDateFiler.toISOString().split("T")[0];
			couponDateFiler = toProperSQLDateString($scope.couponDateFiler);
		}
		
		if ($scope.couponPriceFilter) {
			couponPriceFilter = $scope.couponPriceFilter
		}

		var queryData = {couponTypeFilter : couponTypeFilter,
				couponDateFiler : couponDateFiler,
				couponPriceFilter : couponPriceFilter};
		
		
		promiseGet = CustomerSrvc.getCouponsByQuery(queryData);
		
		promiseGet.then(
				function (resp){					
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						$scope.coupons = resp.data;
					}

				},
				
				function (err){
					alert(err.data);
				}
			)
	}//OF getFilteredList
	
	
	$scope.purchaseCoupon = function (coupon) {
		
		var promisePost = CustomerSrvc.purchaseCoupon(coupon);
		
		promisePost.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						growl.success(coupon.title + " purchased successfully");
						$location.url('get-customer-coupons');
					}
				},
				
				function (err){			
					alert(err.data);
				}
			)
			
	}
	
}


function populateCouponTable($scope, CustomerSrvc, $window, growl, $location){
	
	var promiseGet = CustomerSrvc.getAllCoupons();
	
	promiseGet.then(
		function (resp){
			if (resp.data.error) {
				growl.error(resp.data.error);
			}
			else if (resp.data.redirectToLogin) {
				$window.location.href = "../";
			}
			else {
				$scope.coupons = resp.data;
			}

		},
		function (err){
			alert(err.data);
		}
	)
	
}