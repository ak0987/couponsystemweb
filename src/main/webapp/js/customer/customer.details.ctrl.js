app.controller("CustomerDetailsCtrl", ['$scope', 'CustomerSrvc', '$window', 'growl', CustomerDetailsCtrllCtor]);

function CustomerDetailsCtrllCtor($scope, CustomerSrvc, $window, growl){
	
	var promiseGet = CustomerSrvc.getCustomerName();
	
	promiseGet.then(
		function (resp){
				$scope.customerName = resp.data.customerName;
		},
		
		function (err){			
			alert(err.data);
		}
	)
	
	
}