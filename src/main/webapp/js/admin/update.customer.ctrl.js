app.controller("UpdateCustomerCtrl", ['$scope', 'AdminSrvc', '$stateParams', '$location', '$window', 'growl', UpdateCustomerCtrlCtor]);

// Ctor method
function UpdateCustomerCtrlCtor($scope, AdminSrvc, $stateParams, $location, $window, growl){
	
	$scope.updateCustomer = function () {
		
		var customer = $scope.customer;
		
		
		if (customer.password == $scope.confirmPassword) {
			
			var promisePost = AdminSrvc.updateCustomer(customer);
			
			promisePost.then(
					function (resp){
						if (resp.data.error) {
							growl.error(resp.data.error);
						}
						else if (resp.data.redirectToLogin) {
							$window.location.href = "../";
						}
						else {
							$location.url('get-all-customers');
						}
					},
					
					function (err){			
						alert(err.data);
					}
				)
		}
		else {
			growl.error("Passwords do not match!");
		}

	}
	
}