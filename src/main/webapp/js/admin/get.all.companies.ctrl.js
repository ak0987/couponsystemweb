	//var module = angular.module("adminApp");
	
	app.controller("GetAllCompaniesCtrl", ['$scope', 'AdminSrvc', '$window', 'growl', GetAllCompaniesCtrlCtor]);
	
	// Ctor method
	function GetAllCompaniesCtrlCtor($scope, AdminSrvc, $window, growl){
		
		$scope.companies = [];		
		
		populateCompaniesTable($scope, AdminSrvc, $window, growl);
		
	}
	
	
	function populateCompaniesTable($scope, AdminSrvc, $window, growl){
		
		var promiseGet = AdminSrvc.getAllCompanies();
		
		promiseGet.then(
			function (resp){
				if (resp.data.error) {
					growl.error(resp.data.error);
				}
				else if (resp.data.redirectToLogin) {
					$window.location.href = "../";
				}
				else {
					$scope.companies = resp.data;
				}

			},
			function (err){
				alert(err.data);
			}
		)
		
	}
	
	
