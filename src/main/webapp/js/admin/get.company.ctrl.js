//var module = angular.module("companyApp");
	
app.controller("GetCompanyCtrl", ['$scope', 'AdminSrvc', '$stateParams', '$location', '$window', 'growl', GetCompanyCtrlCtor]);

// Ctor method
function GetCompanyCtrlCtor($scope, AdminSrvc, $stateParams, $location, $window, growl){
	
	this.company = {};

	var promiseGet = AdminSrvc.getCompany($stateParams.id);
	
	promiseGet.then(
		function (resp){
			
			if (resp.data.error) {
				growl.error(resp.data.error);
			}
			else if (resp.data.redirectToLogin) {
				$window.location.href = "../";
			}
			else {
				$scope.company = resp.data;
				$scope.confirmPassword = $scope.company.password;
			}

		},
		
		function (err){			
			alert(err.data);
		}
	)

}