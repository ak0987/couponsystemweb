app.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
}]);

// router config
app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state("getAllCompanies", {
        url: "/get-all-companies",
        templateUrl: "get-all-companies.html",
        controller: "GetAllCompaniesCtrl"
    })
    .state("getCompany", {
        url: "/get-company?id",
        templateUrl: "get-company.html",
        controller: "GetCompanyCtrl"
    })
    .state("updateCompany", {
        url: "/update-company?id",
        templateUrl: "update-company.html",
    })
    .state("removeCompany", {
        url: "/remove-company?id",
        templateUrl: "remove-company.html",
    })
    .state("createCompany", {
        url: "/create-company",
        templateUrl: "create-company.html",
        controller: "CreateCompanyCtrl"
    })
    
    .state("getAllCustomers", {
        url: "/get-all-customers",
        templateUrl: "get-all-customers.html",
        controller: "GetAllCustomersCtrl"
    })
    .state("getCustomer", {
        url: "/get-customer?id",
        templateUrl: "get-customer.html",
        controller: "GetCustomerCtrl"
    })
    .state("updateCustomer", {
        url: "/update-customer?id",
        templateUrl: "update-customer.html",
    })
    .state("removeCustomer", {
        url: "/remove-customer?id",
        templateUrl: "remove-customer.html",
    })
    .state("createCustomer", {
        url: "/create-customer",
        templateUrl: "create-customer.html",
        controller: "CreateCustomerCtrl"
    });
    
});