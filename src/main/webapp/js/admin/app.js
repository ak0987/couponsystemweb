var app = angular.module("adminApp", ["ui.router", "angular-growl", "ngCookies"]);

app.config(['growlProvider', function(growlProvider) {
  growlProvider.globalTimeToLive(5000);
  growlProvider.globalDisableCountDown(true);
}]);