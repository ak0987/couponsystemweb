//var module = angular.module("companyApp");

app.service("AdminSrvc", AdminSrvcCtor);

function AdminSrvcCtor($http)
{
	
	this.getAllCompanies = function() {
		return $http.get("../webapi/admin/get-all-companies");		
	}
	
	this.getCompany = function(companyId) {
		return $http.get("../webapi/admin/get-company?id=" + companyId);		
	}
	
	this.createCompany = function(company) {
		return $http.post("../webapi/admin/create-company", company);		
	}
	
	this.updateCompany = function(company) {
		return $http.put("../webapi/admin/update-company", company);		
	}
	
	this.removeCompany = function(companyId) {
		return $http.delete("../webapi/admin/remove-company?id=" + companyId);		
	}
	
	
	this.getAllCustomers = function() {
		return $http.get("../webapi/admin/get-all-customers");		
	}
	
	this.getCustomer = function(customerId) {
		return $http.get("../webapi/admin/get-customer?id=" + customerId);		
	}
	
	this.createCustomer = function(customer) {
		return $http.post("../webapi/admin/create-customer", customer);		
	}
	
	this.updateCustomer = function(customer) {
		return $http.put("../webapi/admin/update-customer", customer);		
	}
	
	this.removeCustomer = function(customerId) {
		return $http.delete("../webapi/admin/remove-customer?id=" + customerId);		
	}

	
}