app.controller("RemoveCustomerCtrl",['$scope', 'AdminSrvc', '$location', '$window', 'growl', RemoveCustomerCtrlCtor]);

// Ctor method
function RemoveCustomerCtrlCtor($scope, AdminSrvc, $location, $window, growl){
	

	$scope.removeCustomer = function () {
		
		
		var promisePost = AdminSrvc.removeCustomer($scope.customer.id);
				
		promisePost.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						growl.success("Customer deleted.");
						$location.url('get-all-customers');
					}
				},
				
				function (err){			
					alert(err.data);
				}
			)
		
		
	}

}