app.controller("UploadCtrl", ['$scope', 'Upload', UploadCtrlCtor]);

// Ctor method
function UploadCtrlCtor($scope, Upload){

  // upload on file select or drop
  $scope.upload = function (file) {
      Upload.upload({
          url: 'webapi/upload-file',
          data: {file: file, couponId: $scope.coupon.id}
      }).then(function (resp) {
    	  $scope.couponThumb = resp.data; //Save file name for later (possible) use
    	  $scope.$emit("ImageChanged", resp.data);
          console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
      }, function (resp) {
          console.log('Error status: ' + resp.status);
      }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
      });
  };

}