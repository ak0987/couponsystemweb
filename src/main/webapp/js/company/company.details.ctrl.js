app.controller("CompanyDetailsCtrl", ['CompanySrvc', '$scope', CompanyDetailsCtrlCtor]);

function CompanyDetailsCtrlCtor(CompanySrvc, $scope){
	
	var promiseGet = CompanySrvc.getCompanyName();
	
	promiseGet.then(
		function (resp){
			
			$scope.companyName = resp.data.companyName;

		},
		
		function (err){			
			alert(err.data);
		}
	)
	
	
}