app.controller("CreateCompanyCtrl", ['$scope', 'AdminSrvc', '$stateParams', '$location', '$window', 'growl', CreateCompanyCtrlCtor]);

// Ctor method
function CreateCompanyCtrlCtor($scope, AdminSrvc, $stateParams, $location, $window, growl){
	
	
	$scope.createCompany = function () {
		
		var company = $scope.company;
		
		if (company.password == $scope.confirmPassword) {
			
			var promisePost = AdminSrvc.createCompany(company);
			
			promisePost.then(
					function (resp){
						if (resp.data.error) {
							growl.error(resp.data.error);
						}
						else if (resp.data.redirectToLogin) {
							$window.location.href = "../";
						}
						else {
							$location.url('get-all-companies');
						}
					},
					
					function (err){			
						alert(err.data);
					}
				)
		}
		else {
			growl.error("Passwords do not match!");
		}
		


	}
	
	
}