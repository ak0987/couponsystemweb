app.controller("GetCustomerCtrl", ['$scope', 'AdminSrvc', '$stateParams', '$location', '$window', 'growl', GetCustomerCtrlCtor]);

// Ctor method
function GetCustomerCtrlCtor($scope, AdminSrvc, $stateParams, $location, $window, growl){
	
	this.customer = {};

	var promiseGet = AdminSrvc.getCustomer($stateParams.id);
	
	promiseGet.then(
		function (resp){
			
			if (resp.data.error) {
				growl.error(resp.data.error);
			}
			else if (resp.data.redirectToLogin) {
				$window.location.href = "../";
			}
			else {
				$scope.customer = resp.data;
				$scope.confirmPassword = $scope.customer.password;
			}

		},
		
		function (err){			
			alert(err.data);
		}
	)

}