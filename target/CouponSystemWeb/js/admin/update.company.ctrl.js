//var module = angular.module("companyApp");
	
app.controller("UpdateCompanyCtrl", ['$scope', 'AdminSrvc', '$stateParams', '$location', '$window', 'growl', UpdateCompanyCtrlCtor]);

// Ctor method
function UpdateCompanyCtrlCtor($scope, AdminSrvc, $stateParams, $location, $window, growl){
	
	$scope.updateCompany = function () {
		
		var company = $scope.company;
		
		if (company.password == $scope.confirmPassword) {
			
			var promisePost = AdminSrvc.updateCompany(company);
			
			promisePost.then(
					function (resp){
						if (resp.data.error) {
							growl.error(resp.data.error);
						}
						else if (resp.data.redirectToLogin) {
							$window.location.href = "../";
						}
						else {
							$location.url('get-all-companies');
						}
					},
					
					function (err){			
						alert(err.data);
					}
				)
		}
		else {
			growl.error("Passwords do not match!");
		}
		
	}
	
	
}