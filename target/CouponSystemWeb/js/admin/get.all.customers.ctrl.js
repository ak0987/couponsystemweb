app.controller("GetAllCustomersCtrl", ['$scope', 'AdminSrvc', '$window', 'growl', GetAllCustomersCtrlCtor]);

// Ctor method
function GetAllCustomersCtrlCtor($scope, AdminSrvc, $window, growl){
	
	$scope.customers = [];		
	
	populateCustomersTable($scope, AdminSrvc, $window, growl);
	
}


function populateCustomersTable($scope, AdminSrvc, $window, growl){
	
	var promiseGet = AdminSrvc.getAllCustomers();
	
	promiseGet.then(
		function (resp){
			if (resp.data.error) {
				growl.error(resp.data.error);
			}
			else if (resp.data.redirectToLogin) {
				$window.location.href = "../";
			}
			else {
				$scope.customers = resp.data;
			}

		},
		function (err){
			alert(err.data);
		}
	)
	
}