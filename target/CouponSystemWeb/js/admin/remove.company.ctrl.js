app.controller("RemoveCompanyCtrl", ['$scope', 'AdminSrvc', '$location', '$window', 'growl', RemoveCompanyCtrlCtor]);

// Ctor method
function RemoveCompanyCtrlCtor($scope, AdminSrvc, $location, $window, growl){
	

	$scope.removeCompany = function () {
		
		
		var promisePost = AdminSrvc.removeCompany($scope.company.id);
				
		promisePost.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						growl.success("Company deleted.");
						$location.url('get-all-companies');
					}
				},
				
				function (err){			
					alert(err.data);
				}
			)
		
		
	}

}
	

	
	
	