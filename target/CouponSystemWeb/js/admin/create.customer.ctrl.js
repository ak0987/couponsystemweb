app.controller("CreateCustomerCtrl", ['$scope', 'AdminSrvc', '$stateParams', '$location', '$window', 'growl', CreateCustomerCtrlCtor]);

// Ctor method
function CreateCustomerCtrlCtor($scope, AdminSrvc, $stateParams, $location, $window, growl){
	
	
	$scope.createCustomer = function () {
		
		var customer = $scope.customer;
		
		if (customer.password == $scope.confirmPassword) {
			
			var promisePost = AdminSrvc.createCustomer(customer);
			
			promisePost.then(
					function (resp){
						if (resp.data.error) {
							growl.error(resp.data.error);
						}
						else if (resp.data.redirectToLogin) {
							$window.location.href = "../";
						}
						else {
							$location.url('get-all-customers');
						}
					},
					
					function (err){			
						alert(err.data);
					}
				)
		}
		else {
			growl.error("Passwords do not match!");
		}
		


	}
	
	
}