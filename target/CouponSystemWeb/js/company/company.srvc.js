app.service("CompanySrvc", CompanySrvcCtor);

function CompanySrvcCtor($http)
{
	
	this.getAllCoupons = function() {
		return $http.get("../webapi/company/get-all-coupons");		
	}
	
	this.getCouponsByQuery = function(queryData) {
		return $http.get("../webapi/company/get-coupons-list-query", {params : queryData});		
	}
	
	this.getCoupon = function(couponId) {
		return $http.get("../webapi/company/get-coupon?id=" + couponId);		
	}
	
	this.createCoupon = function(coupon) {
		return $http.post("../webapi/company/create-coupon", coupon);		
	}
	
	this.updateCoupon = function(coupon) {
		return $http.put("../webapi/company/update-coupon", coupon);		
	}
	
	
	this.getCompanyName = function() {
		return $http.get("../webapi/company/get-company-name");		
	}
	
	this.removeCoupon = function(couponId) {
		return $http.delete("../webapi/company/remove-coupon?id=" + couponId);		
	}

	
}