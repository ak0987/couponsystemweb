var app = angular.module("companyApp", ["ui.router", "ngFileUpload", "angular-growl", "ngCookies"]);

app.config(['growlProvider', function(growlProvider) {
  growlProvider.globalTimeToLive(5000);
  growlProvider.globalDisableCountDown(true);
}]);