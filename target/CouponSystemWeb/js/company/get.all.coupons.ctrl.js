app.controller("GetAllCouponsCtrl", ['CompanySrvc', '$scope', '$window', 'growl', GetAllCouponsCtrlCtor]);

// Ctor method
function GetAllCouponsCtrlCtor(CompanySrvc, $scope, $window, growl){
	
	$scope.coupons = [];		
	
	populateCouponTable(CompanySrvc, $scope, $window, growl);
	

	$scope.getAllCoupons = function () {
		$scope.couponTypeFilter = "";
		$scope.couponDateFiler = null;
		$scope.couponPriceFilter = "";
		populateCouponTable(CompanySrvc, $scope, $window, growl);
	}
	
	$scope.getCouponListByQuery = function () {
		var couponTypeFilter = "";
		var couponDateFiler = "";
		var couponPriceFilter = "-1";
		
		if ($scope.couponTypeFilter) {
			couponTypeFilter = $scope.couponTypeFilter
		}
		
		if ($scope.couponDateFiler) {
			couponDateFiler = toProperSQLDateString($scope.couponDateFiler);
		}
		
		if ($scope.couponPriceFilter) {
			couponPriceFilter = $scope.couponPriceFilter
		}

		var queryData = {couponTypeFilter : couponTypeFilter,
				couponDateFiler : couponDateFiler,
				couponPriceFilter : couponPriceFilter};
		
		
		promiseGet = CompanySrvc.getCouponsByQuery(queryData);
		
		promiseGet.then(
				
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						$scope.coupons = resp.data;
					}

				},
				function (err){
					alert('error');
					alert(err.data);
				}
			)
	}//OF getFilteredList
	
	
}


function populateCouponTable(CompanySrvc, $scope, $window, growl){
	
	var promiseGet = CompanySrvc.getAllCoupons();
	
	promiseGet.then(
		function (resp){
			if (resp.data.error) {
				growl.error(resp.data.error);
			}
			else if (resp.data.redirectToLogin) {
				$window.location.href = "../";
			}
			else {
				$scope.coupons = resp.data;
			}

		},
		function (err){
			alert(err.data);
		}
	)
	
}