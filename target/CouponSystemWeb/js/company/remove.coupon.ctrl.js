app.controller("RemoveCouponCtrl", ['$window', 'growl', RemoveCouponCtrlCtor]);

// Ctor method
function RemoveCouponCtrlCtor($scope, CompanySrvc, $stateParams, $location, $window, growl){
	

	$scope.deleteCoupon = function () {
		
		
		var promisePost = CompanySrvc.removeCoupon($stateParams.id);
				
		promisePost.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						$location.url('get-all-coupons');
					}
				},
				
				function (err){			
					alert(err.data);
				}
			)
		
		
	}

}
	

	
	
	