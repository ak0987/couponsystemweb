app.controller("UpdateCouponCtrl", ['$scope', 'CompanySrvc', '$stateParams', '$location', 'Upload',  '$window', 'growl', UpdateCouponCtrlCtor]);

// Ctor method
function UpdateCouponCtrlCtor($scope, CompanySrvc, $stateParams, $location, Upload, $window, growl){
	
	//Indicate that coupon image was updated/added by Upload controller (child)
	var imageChanged = false;
	var imageNewName = "";
	

	//Listen for event from Uploader controller (child) that indicates that coupon image has been changed
	$scope.$on("ImageChanged", function(evt,data){ 
		imageChanged = true;
		imageNewName = data;
	});
	
	$scope.updateCoupon = function () {
		
		var coupon = $scope.coupon;
		coupon.startDate = toProperSQLDateString($scope.startDate);
		coupon.endDate = toProperSQLDateString($scope.endDate);
		
		//Add indication that image was changed during edit, without changing the model too much
		if (imageChanged) {
			coupon.image = coupon.image + "$" + imageNewName;
		}
		
		
		var promisePost = CompanySrvc.updateCoupon(coupon);
		
		promisePost.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						$location.url('get-all-coupons');
					}
				},
				
				function (err){			
					alert(err.data);
				}
			)
			
	}
}