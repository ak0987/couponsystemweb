app.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
}]);

// router config
app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state("getAllCoupons", {
        url: "/get-all-coupons",
        templateUrl: "get-all-coupons.html",
        controller: "GetAllCouponsCtrl as getAll"
    })
    .state("getCoupon", {
        url: "/coupon-details?id",
        templateUrl: "get-coupon.html",
        controller: "GetCouponCtrl as getCoup"
    })
    .state("updateCoupon", {
        url: "/update-coupon?id",
        templateUrl: "update-coupon.html",
        //controller: "GetCouponCtrl as editCoup"
    })
    .state("removeCoupon", {
        url: "/remove-coupon?id",
        templateUrl: "remove-coupon.html",
        //controller: "RemoveCouponCtrl as removeCoup"
    })
    .state("createCoupon", {
        url: "/create-coupon",
        templateUrl: "create-coupon.html",
        controller: "CreateCouponCtrl as createCoup"
    });
    
});