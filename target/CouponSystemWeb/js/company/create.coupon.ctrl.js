app.controller("CreateCouponCtrl", ['$scope', 'CompanySrvc', '$stateParams', '$location', 'Upload', '$window', 'growl', CreateCouponCtrlCtor]);

// Ctor method
function CreateCouponCtrlCtor($scope, CompanySrvc, $stateParams, $location, Upload, $window, growl){
	
	
	//Indicate that coupon image was updated/added by Upload controller (child)
	var imageChanged = false;

	//Listen for event from Uploader controller (child) that indicates that coupon image has been changed
	$scope.$on("ImageChanged", function(evt,data){ 
		imageChanged = true;
		imageNewName = data;
	});
	
	
	$scope.createCoupon = function () {
		
		var coupon = $scope.coupon;
		
		coupon.startDate = toProperSQLDateString($scope.startDate);
		coupon.endDate = toProperSQLDateString($scope.endDate);
		
		//Add indication that image was changed during edit, without changing the model too much
		if (imageChanged) {
			coupon.image = "$" + imageNewName;
		}
		else {
			coupon.image = "";
		}
		
		
		var promisePost = CompanySrvc.createCoupon(coupon);
		
		promisePost.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						$location.url('get-all-coupons');
					}
				},
				
				function (err){			
					alert(err.data);
				}
			)

	}
	
	
      
      // upload on file select or drop
      $scope.upload = function (file) {
          Upload.upload({
              url: '../webapi/upload-file',
              data: {file: file, couponId: $scope.coupon.id}
          }).then(function (resp) {
        	  $scope.couponThumb = resp.data; //Save file name for later (possible) use
        	  thumbChanged = true;
              console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
          }, function (resp) {
              console.log('Error status: ' + resp.status);
          }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
          });
      };
	
}