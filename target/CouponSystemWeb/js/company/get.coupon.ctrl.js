app.controller("GetCouponCtrl", ['$scope', 'CompanySrvc', '$stateParams', '$location', 'Upload',  '$window', 'growl', GetCouponCtrlCtor]);

// Ctor method
function GetCouponCtrlCtor($scope, CompanySrvc, $stateParams, $location, Upload, $window, growl){
	
	this.coupon = {};

	var promiseGet = CompanySrvc.getCoupon($stateParams.id);
	
	promiseGet.then(
		function (resp){
			
			if (resp.data.error) {
				growl.error(resp.data.error);
			}
			else if (resp.data.redirectToLogin) {
				$window.location.href = "../";
			}
			else {
				$scope.coupon = resp.data;
				$scope.couponImage = $scope.coupon.image;
				$scope.startDate = new Date($scope.coupon.startDate);
				$scope.endDate = new Date($scope.coupon.endDate);
			}

		},
		
		function (err){			
			alert(err.data);
		}
	)

}