app.controller("GetCustomerCouponsCtrl", ['$scope', 'CustomerSrvc',  '$window', 'growl', '$location', GetCustomerCouponsCtrlCtor]);

// Ctor method
function GetCustomerCouponsCtrlCtor($scope, CustomerSrvc, $window, growl, $location){
	
	$scope.coupons = [];		
	
	populateCustomerCouponTable($scope, CustomerSrvc, $window, growl, $location);
	

	$scope.getAllCustomerCoupons = function () {
		$scope.couponTypeFilter = "";
		$scope.couponDateFiler = null;
		$scope.couponPriceFilter = "";
		populateCustomerCouponTable($scope, CustomerSrvc, $window, growl, $location);
	}
	
	$scope.getCustomerCouponListByQuery = function () {
		var couponTypeFilter = "";
		var couponDateFiler = "";
		var couponPriceFilter = "-1";
		
		if ($scope.couponTypeFilter) {
			couponTypeFilter = $scope.couponTypeFilter
		}
		
		if ($scope.couponDateFiler) {
			couponDateFiler = toProperSQLDateString($scope.couponDateFiler);
			var foo = $scope.couponDateFiler.getDate();
		}
		
		if ($scope.couponPriceFilter) {
			couponPriceFilter = $scope.couponPriceFilter
		}

		var queryData = {couponTypeFilter : couponTypeFilter,
				couponDateFiler : couponDateFiler,
				couponPriceFilter : couponPriceFilter};
		
		
		promiseGet = CustomerSrvc.getCustomerCouponsByQuery(queryData);
		
		promiseGet.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else if (resp.data.redirectToLogin) {
						$window.location.href = "../";
					}
					else {
						$scope.coupons = resp.data;
					}

				},
				
				function (err){
					alert(err.data);
				}
			)
	}//OF getCustomerCouponListByQuery
	
	
	
}


function populateCustomerCouponTable($scope, CustomerSrvc, $window, growl, $location){
	
	var promiseGet = CustomerSrvc.getAllPurchasedCoupons();
	
	promiseGet.then(
		function (resp){
			if (resp.data.error) {
				growl.error(resp.data.error);
			}
			else {
				$scope.coupons = resp.data;
			}

		},
		function (err){
			alert(err.data);
		}
	)
	
}