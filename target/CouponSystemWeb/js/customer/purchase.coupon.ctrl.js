app.controller("GetAllPurchasedCouponsCtrl", ['$scope', 'CustomerSrvc', '$stateParams', '$location',  '$window', 'growl', GetAllPurchasedCouponsCtrlCtor]);

// Constructor method
function GetAllPurchasedCouponsCtrlCtor($scope, CustomerSrvc, $stateParams, $location, $window, growl){
	
	this.coupon = {};

	var promiseGet = CustomerSrvc.getAllPurchasedCoupons();
	
	promiseGet.then(
			
			function (resp){
				if (resp.data.error) {
					growl.error(resp.data.error);
				}
				else if (resp.data.redirectToLogin) {
					$window.location.href = "../";
				}
				else {
					$scope.coupon = resp.data;
				}

			},
		
			function (err){			
				alert(err.data);
			}
		)
}