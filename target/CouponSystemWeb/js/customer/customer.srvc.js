app.service("CustomerSrvc", CustomerSrvcCtor);

function CustomerSrvcCtor($http)
{
	
	this.getAllCoupons = function() {
		return $http.get("../webapi/customer/get-all-coupons");		
	}
	
	this.getAllPurchasedCoupons = function() {
		return $http.get("../webapi/customer/get-all-purchased-coupons");		
	}
	
	this.getCoupon = function(couponId) {
		return $http.get("../webapi/customer/get-coupon?id=" + couponId);		
	}
	
	this.purchaseCoupon = function(coupon) {
		return $http.put("../webapi/customer/purchase-coupon", coupon);		
	}
	
	this.getCouponsByQuery = function(queryData) {
		return $http.get("../webapi/customer/get-coupons-list-query", {params : queryData});		
	}
	
	this.getCustomerCouponsByQuery = function(queryData) {
		return $http.get("../webapi/customer/get-customer-coupons-list-query", {params : queryData});		
	}
	
	this.getCustomerName = function() {
		return $http.get("../webapi/customer/get-customer-name");		
	}
	
	
}