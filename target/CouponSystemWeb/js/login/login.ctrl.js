//var module = angular.module("companyApp");
	
app.controller("LoginCtrl", ['$scope', 'LoginSrvc', '$window', 'growl', '$cookies', LoginCtrlCtor]);

// Ctor method
function LoginCtrlCtor($scope, LoginSrvc, $window, growl, $cookies){
	
	
	$scope.login = function () {
		
		if ($scope.loginForm.$valid) {
			
			var promisePost = LoginSrvc.login($scope.loginData);
			
			promisePost.then(
					function (resp){
						if (resp.data.error) {
							growl.error(resp.data.error);
						}
						else {
							$window.location.href = resp.data.url;
							//$location.url(resp.data.url);
							
						}
					},
					function (err){			
						alert(err.data);
					}
				)
		}
		else {
			growl.error("LOGIN DATA MISSING");
		}
		

	}
	
	
	$scope.logout = function () {
		
		var promiseGet = LoginSrvc.logout();
		
		promiseGet.then(
				function (resp){
					if (resp.data.error) {
						growl.error(resp.data.error);
					}
					else {
						$window.location.href = resp.data.url;
						//$location.url(resp.data.url);
						
					}
				},
				function (err){			
					alert(err.data);
				}
			)
	}


}